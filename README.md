# Python CI/CD tanfolyam

## Bevezetés

[Bevezetés](intro.md)

## Saját repo létrehozása

* Közös projekt clone-ozása

```powershell
git clone https://gitlab.com/devops2034/devops3/employees
```

* Saját könyvtár, saját projekt létrehozása

```powershell
employees-vicziani
```

* Ebbe a könyvtárba át kell másolni a közös projekből a fájlokat
* Lokális repo

```powershell
git init
git add .
git commit -m "Init"
```

* Létrehozni a távoli repo-t
  * Blank project, ne hozza létre a `README.md` állományt
  * Neve: `employees-vicziani`

* Lokális - távoli összekötése

```powershell
git remote add origin https://gitlab.com/devops2034/devops3/cicd/employees-vicziani.git
```

* Push

```powershell
git push origin master
```

## VS Code

* Python extension feltelepítése

## Virtualenv

```powershell
python -m venv venv
```

* Terminál újraindítása
* Ha nem fut le a `ps1` fájl, akkor külön Adminisztrátori PS ablakban:

```powershell
Set-ExecutionPolicy RemoteSigned
```

Újraindítás után a `where.exe python` parancs a `venv` könyvtárban
lévő pythont fogja visszaadni elsőnek.

majd terminál bezárás, újra nyitás Code-on belül

## Függőségek feltelepítése

```powershell
pip install -r .\requirements.txt
```

## Adatbázis Docker konténerben

```powershell
docker run -d -e MYSQL_DATABASE=employees -e MYSQL_USER=employees -e MYSQL_PASSWORD=employees -e MYSQL_ALLOW_EMPTY_PASSWORD=yes -p 3306:3306 --name employees-mysql mysql
```

Tényleg elindult?

```powershell
docker logs -f employees-mysql
```

# Alkalmazás futtatása

```powershell
$env:FLASK_APP='src/employees'
python -m flask run 
```

Az alkalmazás itt érhető el: `http://127.0.0.1:5000`

Tényleg az adatbázisba ír?

```powershell
docker exec -it employees-mysql mysql employees
```

```sql
select * from employees;
```

## Unit tesztek futtatása

* Kell hozzá egy `pytest.ini`

```powershell
pytest -v
```

## Teszt lefedettség mérése

Konzolra:

```powershell
pytest -v --cov employees
```

HTML riportban:

```powershell
pytest -v --cov employees --cov-report=html
```

## Docker image előállítása

* Létre kell hozni egy `Dockerfile`-t

Image készítése:

```powershell
docker build -t employees .
```

## Felkészülés E2E tesztekre

* `pytest.ini`-be a `testpaths = tests` sor elhelyezése, hogy csak a unit teszteket futtassa
* `e2e\docker-compose.yml` állomány átmásolása
* `e2e_tests` könyvtár átmásolása

Ki lehet próbálni, ha elindítjuk így az alkalmazást:

```powershell
cd e2e
docker compose up
```

Az alkalmazás itt érhető el: `http://127.0.0.1:5000`

Utána szakítsuk meg nyugodtan a compose futtatását, mert az e2e teszteket fogjuk futtatni.

* `e2e.Dockerfile` bemásolása a gyökérkönyvtárba

```powershell
docker build -t employees-e2e -f .\e2e.Dockerfile .
```

* `docker-compose.yml` felülírása

* E2E tesztek futtatása

```powershell
docker compose up --abort-on-container-exit
```

Ha a db nem indul el elég gyorsan: https://docs.docker.com/compose/startup-order/

(wait-for-it.sh használata opcionális házi feladat)

## Statikus kódelemzés

```powershell
pylint src employees -r n > pylint-report.txt
```

## Feltöltés GitLab registry-be

```powershell
docker login registry.gitlab.com
docker tag employees registry.gitlab.com/devops2034/devops3/employees
docker push registry.gitlab.com/devops2034/devops3/employees
```

Login esetén ha OAuth 2 autentikáció van (pl. GMail), akkor tokent kell létrehozni,
és azt adni meg felhasználónévnek és jelszónak.

## AWS EC2

FIGYELJ AZ SG, AMI azonosítókra!

```shell
aws ec2 create-key-pair --key-name employeeskey --query 'KeyMaterial' --output text > employeeskey.pem
aws ec2 create-security-group --group-name EmployeesSecurityGroup --description "EmployeesSecurityGroup"
aws ec2 authorize-security-group-ingress --port 22 --protocol tcp --group-id sg-081c88350e9f551b5 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --port 80 --protocol tcp --group-id sg-081c88350e9f551b5 --cidr 0.0.0.0/0
aws ec2 run-instances --image-id ami-0caef02b518350c8b --count 1 --instance-type t2.micro --key-name employeeskey --security-group-ids sg-081c88350e9f551b5

aws ec2 describe-instances --instance-ids i-0c9c9b44b --query 'Reservations[*].Instances[*].PublicIpAddress' --output text

ssh -i employeeskey.pem ubuntu@18.156.163.44
```

![Stackoverflow](https://pbs.twimg.com/media/Fg_KL-ZWAAMXUDo.jpg)

```shell
sudo apt-get update
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

Teszt: 

```
sudo docker run hello-world
```

Alkalmazás elindítása:

```
sudo docker network create employees
sudo docker run -d -e MYSQL_DATABASE=employees -e MYSQL_USER=employees -e MYSQL_PASSWORD=employees -e MYSQL_ALLOW_EMPTY_PASSWORD=yes -p 3306:3306 --name employees-mysql --network employees mysql
sudo docker login registry.gitlab.com
sudo docker run -d -p 80:5000 -e DATABASE_HOST=employees-mysql --network employees --name employees registry.gitlab.com/devops2034/devops3/employees
```

Ne kelljen sudo:

```
sudo usermod -G docker ubuntu
groups ubuntu
```

# GitLab

* A `.gitlab-ci.yml` fájl átmásolása
* A `ID_RSA`, `SERVER_USER`, `SERVER_IP` beállítása a GitLab felületén, ezt a _Settings / CI/CD / Variables_ menüpontban kell ezt megtenni.
A `SERVER_IP` az EC2 példány publikus ip-címe. A `SERVER_USER` az `ubuntu`, ez a felhasználó mellyel a bejelentkezés megtörténik. Az `ID_RSA`
legyen `File` típusú, és az EC2 példányhoz generált kulcs publikus részét (pem állomány) kell ide bemásolni.


# Saját GitLab Runner futtatása

A következő paranccsal indíts egy saját Runnert!

```shell
docker run -d -v /var/run/docker.sock:/var/run/docker.sock --name my-gitlab-runner gitlab/gitlab-runner:alpine
```

Utána ezt regisztráld a következő módon:

* Projekt felületén _Settings / CI/CD_
* Ott _Runners_
* _Specific runners_ oldalon ki kell másolni a tokent
* Add ki a következő parancsot, de cseréld ki a `GR1348941Qy_xPxyxkZsGDAL1LZnW` értéket a saját tokenedre, valamint a nevet `runner-vicziani` értékről úgy, hogy a saját neved szerepeljen benne

```shell
docker exec -it my-gitlab-runner gitlab-runner register --url https://gitlab.com --registration-token GR1348941Qy_xPxyxkZsGDAL1LZnW --non-interactive --executor docker --description runner-vicziani --docker-image docker:latest
```

* Ekkor frissítsd az oldalt, és megjelenik bal oldalt a beregisztrált runner
* Jobb oldalt a _Enable shared runners for this project_ kapcsolót kapcsold ki! Valamint ez alatt a _Disable group runners_ gombot is meg kell nyomni.

Ha így indítasz egy Pipeline-t, akkor már ezen a Runneren fog futni. Ez a Job naplójának elején is látszik:

```
Running with gitlab-runner 14.10.1 (f761588f)
  on runner-vicziani KrH1tStq
```

## Házi feladat

* A létező EC2 példányon állítsd le a `employees` Docker konténert!
* Hozz létre egy új projektet `web-vicziani` névhez hasonlatosan a te neveddel!
* Hozz létre benne `site` könyvtárat, abban egy `index.html` oldalt, és töltsd fel tetszőleges tartalommal!
* Hozz létre egy saját `Dockerfile`-t. Indulj ki egy NGINX webszerverből. Másold fel az NGINX Docker Hub dokumentációja alapján a megfelelő helyre a `site` könyvtár tartalmát!
* Buildeld le a konténert, próbáld ki, majd töltsd fel a project Container Registry-jébe!
* Írj egy kétlépéses Pipeline-t! Az első lépés állítsa össze az image-t, majd pusholja! A második lépés telepítse ki az EC2 példányra! A konténer neve legyen `my-web`! Ugyanúgy a 80-as portra hozd ki, így azonnal láthatod az eredményt. (Gondoskodj róla, hogy az előző konténert tényleg leállítottad, különben portütközés lesz!)
