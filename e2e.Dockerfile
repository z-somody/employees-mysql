FROM python
WORKDIR /app
COPY e2e_tests e2e_tests
COPY requirements.txt .
RUN pip install -r requirements.txt
ENTRYPOINT ["pytest", "-v"]

