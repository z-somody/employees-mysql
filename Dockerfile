FROM python
WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY src .
ENV FLASK_APP=src/employees
ENTRYPOINT ["python", "-m", "flask", "run", "--host", "0.0.0.0"]
